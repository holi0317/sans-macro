# sans-macros
Keyboard macros for fighting with [sans, the skeleton](http://undertale.wikia.com/wiki/Sans)

Yes this is a freaking cheat. But he is very tough to be fair.

## Installation
This assumes using Linux as operating system.

Install python 3.7 (or above) before running the following commands.

```bash
pip install -r requirements.txt
```

## Run
This script need to be run as root due to constrain in `keyboard` library.

```bash
export FLASK_APP=flaskr
sudo --preserve-env=FLASK_APP,FLASK_ENV flask run
```
