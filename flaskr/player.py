from threading import Thread
from time import sleep
from typing import List, Callable

from .stroke import Stroke

def player(sequence: List[Stroke], cb: Callable[[], None]):
    """
    Play given macro sequence.

    Assumption: sequence is sorted by rel_time, in increasing order.

    Param:
        sequence - List of strokes to be played
        cb - Callback to be invoked when macro has finished.
            Called in Player thread.
    """
    last = 0.0

    for stroke in sequence:
        sleep(stroke.rel_time - last)
        stroke.play()
        last = stroke.rel_time

    cb()

