from flask import Flask, g

app = Flask(__name__)
app.secret_key = 'i9yXOx;Ihb7Z#s;"'


@app.teardown_appcontext
def teardown(exception):
    is_recording = getattr(g, 'is_recording', False)
    if is_recording:
        # TODO Stop recording
        pass


from . import views
