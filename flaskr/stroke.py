import keyboard
from typing import Dict, Any


class Stroke(object):
    """
    Defines a stroke in keyboard.

    A stroke could be down (press) or up (release).

    Properties:
        scan_code - Scan code of this stroke
        down - True if this stroke is pressing down
        rel_time - Relative time for this stroke to happen
            (as float, in seconds)
    """

    def __init__(self, scan_code: int, down: bool, rel_time: float) -> None:
        assert rel_time >= 0, 'Relative time of a stroke must not be negative'

        self.scan_code = scan_code
        self.down = down
        self.rel_time = rel_time

    def play(self) -> None:
        """
        Play this key stroke.

        This function does not account for the relative time of this stroke.
        """
        if self.down:
            keyboard.press(self.scan_code)
        else:
            keyboard.release(self.scan_code)

    @classmethod
    def from_json(cls, data: Dict[str, Any]) -> 'Stroke':
        """
        Desearize data from json.
        """
        return cls(
            scan_code=data['scan_code'],
            down=data['down'],
            rel_time=data['rel_time'])

    def to_json(self) -> Dict[str, Any]:
        """
        Serialize this instance to dict that could be further serialized
        by json.
        """
        return {
            'scan_code': self.scan_code,
            'down': self.down,
            'rel_time': self.rel_time
        }
