import uuid

from flask import render_template, flash, url_for, \
    request, abort, redirect
import keyboard

from . import app
from . import macro_manager
from .macro_manager import Macro

state = {'status': 'idle'}


@app.route('/')
def root():
    macros = macro_manager.ls()
    status = state.get('status', 'idle')
    return render_template('root.html', macros=macros, status=status)


@app.route('/new-recording', methods=['POST'])
def new():
    action = request.form['action']
    status = state.get('status', 'idle')

    if action == 'start':
        if status == 'idle':
            flash('Starting recording')
            state['status'] = 'recording'
            keyboard.start_recording()
        else:
            flash(
                'Recording cannot be started when macro is playing or recording'
            )

    elif action == 'save':
        if status == 'recording':
            state['status'] = 'idle'

            res = keyboard.stop_recording()
            name = str(uuid.uuid4())
            macro = Macro.from_record(name, res)
            macro.save()
            flash(f'Saved recording as {name}')
        else:
            flash('Recording cannot be saved.')

    elif action == 'discard':
        if status == 'recording':
            flash('Discarding recording')
            state['status'] = 'idle'
            keyboard.stop_recording()
        else:
            flash('Cannot discard recording')

    else:
        abort(400)

    return redirect(url_for('root'))


@app.route('/remove', methods=['POST'])
def remove_recording():
    name = request.form.get('value', None)
    if name is None:
        abort(400)

    match = macro_manager.find(name)
    if match is None:
        abort(404)

    match.delete()
    flash(f'Removed macro {name}')

    return redirect(url_for('root'))


@app.route('/replay', methods=['POST'])
def replay_recording():
    status = state.get('status', 'idle')
    name = request.form.get('value', None)
    if status != 'idle':
        abort(400, 'Macro could only be replayed when system is in idle')

    macro = macro_manager.find(name)
    if macro is None:
        abort(404)

    state['status'] = 'replaying'
    macro.play(on_macro_ended)
    flash(f'Playing macro {macro.name}')

    return redirect(url_for('root'))


def on_macro_ended() -> None:
    state['status'] = 'idle'


@app.route('/rename', methods=['POST'])
def rename_recording():
    name = request.form.get('value', None)
    new_name = request.form.get('name', None)
    if name is None or new_name is None:
        abort(400)

    match = macro_manager.find(name)
    if match is None:
        abort(404)

    if macro_manager.find(new_name) is not None:
        abort(400, 'New name conflict with existing macro name')

    match.rename(new_name)
    flash(f'Renamed macro from {name} to {new_name}')

    return redirect(url_for('root'))
