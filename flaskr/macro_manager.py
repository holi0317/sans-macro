import os
from pathlib import Path
import json
from threading import Thread
from typing import List, Optional, Callable, Dict, Any

from keyboard import KeyboardEvent

from .stroke import Stroke
from .player import player

MACRO_DIR: Path = Path(__file__).resolve().parent / 'macros'
# Whitelist keys: z, x, c, arrows
WHITELIST_SCAN_CODE = set([0x2c, 0x2d, 0x2e, 0x67, 0x69, 0x6A, 0x6C])

if not MACRO_DIR.is_dir():
    MACRO_DIR.mkdir()


class Macro(object):
    """
    An recorded macro of keyboard sequences

    Attributes:
        sequences - None or List[Stroke]. Sequence of this macro.
        name - str. File name of this macro
    """

    def __init__(self, name: str) -> None:
        self.name = name
        self.sequences = None

    def to_json(self) -> Dict[str, Any]:
        """
        Serialize data of this instance to dict.
        """
        return {'sequences': [stroke.to_json() for stroke in self.sequences]}

    @classmethod
    def from_json(cls, path: Path) -> 'Macro':
        """
        Desearize macro from file on disk.

        Param:
            path - Path pointing to the save file
        """
        instance = cls(path.stem)
        with path.open() as file:
            content = json.load(file)

        instance.sequences = [
            Stroke.from_json(data) for data in content['sequences']
        ]

        return instance

    @classmethod
    def from_record(cls, name: str, record: List[KeyboardEvent]) -> 'Macro':
        """
        Create an instance of macro from recorded events

        Assumption: given record is sorted by time in increasing order

        Param:
            name - Name of the recording file
            record - Keyboard event sequence received from keyboard library
        """
        instance = cls(name)
        if len(record) == 0:
            instance.sequences = []
            return instance

        base_time = record[0].time
        instance.sequences = [
            Stroke(event.scan_code, event.event_type == 'down',
                   event.time - base_time + 3) for event in record
            if event.scan_code in WHITELIST_SCAN_CODE
        ]
        return instance

    def play(self, cb: Callable[[], None] = None) -> None:
        """
        Replay this macro.

        Param:
            cb - The function will be called when playback is completed
        """
        if cb is None:
            cb = lambda: None

        thread = Thread(target=player, args=(self.sequences, cb))
        thread.start()

    def save(self) -> None:
        """
        Write this macro to disk.
        """
        path = MACRO_DIR / (self.name + '.json')
        data = self.to_json()

        with path.open('w') as file:
            json.dump(data, file)

    def delete(self) -> None:
        """
        Delete this macro from disk.

        Note that this object will still be alive after deleting the macro on disk.

        Raise:
            FileNotFoundError - This instance of macro is not found on disk
        """
        path = MACRO_DIR / (self.name + '.json')
        os.remove(path)

    def rename(self, new_name: str) -> None:
        """
        Rename this macro.

        Invoking this function will also change macro name written to disk.

        If this macro is not written to disk, exception will be raised.

        Param:
            new_name - New name of this macro, without file extension

        Raise:
            FileNotFoundError - This instance of macro is not found on disk
        """
        path = MACRO_DIR / (self.name + '.json')
        new_path = MACRO_DIR / (new_name + '.json')
        os.rename(path, new_path)
        self.name = new_name


def ls() -> List[Macro]:
    """
    List all available macros in the system.

    Returns:
        List of macros
    """
    return [Macro.from_json(macro) for macro in MACRO_DIR.iterdir()]


def find(name: str) -> Optional[Macro]:
    """
    Find an macro that is same as the given name.

    Returns:
        Matching macro. If no macro is found, None will be produced.
    """
    path = MACRO_DIR / (name + '.json')
    if not path.is_file():
        return None

    return Macro.from_json(path)
